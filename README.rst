.. image:: https://licensebuttons.net/l/by-sa/4.0/88x31.png
   :target: http://creativecommons.org/licenses/by-sa/4.0/
   :alt: Licence Creative Commons Attribution - Partage dans les Mêmes Conditions 4.0 International


======================
Le Filament - Website
======================

Ceci est le repository contenant le code source du site le-filament.com

Credits
=======

Contributors ------------

* Juliana <juliana@le-filament.com>


Maintainer ----------

.. image:: https://le-filament.com/img/logo-lefilament.png
   :alt: Le Filament
   :target: https://le-filament.com

This module is maintained by Le Filament
